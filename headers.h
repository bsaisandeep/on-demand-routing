#ifndef __HEADERS_H
#define __HEADERS_H

#include <stdint.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>

#define TRUE 1
#define FALSE 0
#define IF_NAME         16      /* same as IFNAMSIZ    in <net/if.h> */
#define IF_HADDR         6      /* same as IFHWADDRLEN in <net/if.h> */
#define IP_ALIAS         1      /* hwa_addr is an alias */
#define RREQ_PKT	1
#define RREP_PKT	2
#define DATA_PKT	3
#define SERVER_PATH "/tmp/srvTmp1321304"
#define ODR_PATH "/tmp/odrTmp1321304"
#define MAXLINE 1024
#define SERVER_PORT 21340
#define PROTOCOL_NUMBER 0x77A5

struct route_entry {
	uint8_t mac_addr[6];
	uint8_t ip_addr[4];
	uint8_t if_index;
	uint8_t hop_count;
	long time_stamp;
	struct route_entry *next;
};

struct rreq_packet {
	uint8_t src_ip_addr[4];
	uint8_t dst_ip_addr[4];
	uint8_t force_discov;
	uint8_t hop_cnt;
	uint8_t bid;
	uint8_t no_redis;
}; 

struct rrep_packet {
        uint8_t src_ip_addr[4];
        uint8_t dst_ip_addr[4];
	uint8_t force_discov;
        uint8_t hop_cnt;
};

struct transprt_packet {
	uint16_t src_port;
	uint16_t dst_port;
	unsigned char payload[400];
};

struct data_packet {
	uint8_t src_ip_addr[4];
	uint8_t dst_ip_addr[4];
	uint8_t force_discov;
	uint8_t hop_cnt;
	struct transprt_packet tpkt;	
};

struct odr_frame {
	uint8_t pkt_type;
	union {
		struct rreq_packet rq_pkt;
		struct rrep_packet rp_pkt;
		struct data_packet dt_pkt;
	} u_field;
};


struct bid_struct {
	uint8_t ip_addr[4];
	uint8_t bid;
	struct bid_struct *next;
};

struct hwa_info {
	char    if_name[IF_NAME];     /* interface name, null terminated */
	uint8_t    if_haddr[IF_HADDR];   /* hardware address */
	int     if_index;             /* interface index */
	short   ip_alias;             /* 1 if hwa_addr is an alias IP address */
	struct  sockaddr  *ip_addr;   /* IP address */
	struct  hwa_info  *hwa_next;  /* next of these structures */
};

struct packet_elements {
	uint8_t dst_mac[6];
	uint8_t src_mac[6];
	uint8_t dst_ip_addr[4];
	uint8_t src_ip_addr[4];
};
	
struct pending_frame {
	unsigned char *frame;
	int processed_flag;
	struct pending_frame *next_frame;
	struct pending_frame *prev_frame;
};

struct port_mapping {
	uint16_t port;
	char file_name[50];
	long time_stamp;
	struct port_mapping *next;
};

/* function prototypes */
struct hwa_info *get_hw_addrs();
struct hwa_info *Get_hw_addrs();
void    free_hwa_info(struct hwa_info *);
int print_ip_addr();
int print_address(uint8_t *addr);
int print_mac_address(uint8_t *addr);
int get_ip_addr(uint8_t *addr);
int pkt_process(int pf_socket, int ud_socket);
int send_data_packet(int sock, unsigned char *buf, char *file_name);
int port_table_lookup(struct port_mapping *prt_map, uint16_t port_num, char *file_name);
int port_table_entry(struct port_mapping **prt_map, char *file_name);
int process_data(unsigned char *buf);
int rreq_packet_process(unsigned char *, struct sockaddr_ll *, int);
int update_route_table(struct route_entry **rtable, uint8_t ip_addr[4], int hop_count,
                uint8_t if_index, uint8_t *mac_addr);
int rreq_broadcast(unsigned char *buf, int if_index, int sock);
int rrep_packet_process(unsigned char *buf, struct sockaddr_ll *socket_address, int sock);
void process_pending_frame(int sock, struct pending_frame **frame_list);
void process_frame(int sock, struct pending_frame *pending_frm);
void enqueue_frame(struct pending_frame **frame_list, unsigned char *buf);
int data_packet_process(unsigned char *buf, struct sockaddr_ll *socket_address, int sock);
int process_header(unsigned char *buf, struct packet_elements *pkt_elem);
int send_packet(int sock, int src_idx, uint8_t dest_mac[6], uint8_t frame_type);
int send_rreq(int sock, uint8_t dst_ip_addr[4], int if_index, uint8_t force_discovery);
int send_rrep_pkt(int sock, unsigned char *buf, int if_index, uint8_t dst_mac_addr[6], uint8_t hop_count);
struct hwa_info  *hwa_data(int if_index);
int fwd_pkt(int sock, unsigned char *buf, int if_index, uint8_t dst_mac_addr[6]);
uint8_t next_bid(struct bid_struct **head);
int bcast_check(struct bid_struct **head, uint8_t bid, uint8_t ip_addr[4]);
struct route_entry *lookup(struct route_entry *head, uint8_t ip_addr[4]);
int comp(uint8_t *addr1, uint8_t *addr2);
void print_packet_info(unsigned char *buf);
void print_rreq_info(unsigned char *buf);
void print_rrep_info(unsigned char *buf);
void print_data_info(unsigned char *buf);
void print_route_table(struct route_entry *rt_entry);
void print_route_entry(struct route_entry *rt_entry);


#endif
