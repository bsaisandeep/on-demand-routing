#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <signal.h>
#include <sys/time.h>
#include "headers.h"


int msg_send(int sockfd, char *destipaddress, int destport, char *msg, int flag){
	int n;
	struct sockaddr_un servaddr;
	struct sockaddr_in sa;	
	struct data_packet *packet;
	uint8_t *start_addr;
	packet = (struct data_packet *) malloc(sizeof(struct data_packet));
	
	bzero(&servaddr, sizeof(servaddr));
	servaddr.sun_family = AF_LOCAL;
	strcpy(servaddr.sun_path, ODR_PATH);

	inet_pton(AF_INET, "0.0.0.0", &(sa.sin_addr));
	start_addr = (uint8_t *) &(sa.sin_addr.s_addr);
	memcpy(packet->src_ip_addr, start_addr, 4);


	inet_pton(AF_INET, destipaddress, &(sa.sin_addr));
	start_addr = (uint8_t *) &(sa.sin_addr.s_addr);
	memcpy(packet->dst_ip_addr, start_addr, 4);	

	packet->tpkt.src_port = htons(0);
	packet->tpkt.dst_port = destport;
	strcpy(packet->tpkt.payload,msg);
	
	packet->force_discov = (uint8_t) flag;
	n = sendto(sockfd, (unsigned char *)packet, sizeof(struct data_packet), 0, (struct sockaddr *)&servaddr, sizeof(servaddr));
	
//	perror("");	
	printf("\nNumber of bytes sent %d\n", n);

	if ( n < 0 )
		printf("\nsend failed in msg_send");
return 0;
}

int msg_recv(int sockfd, char *msg, uint8_t *srcipaddress, int *srcport){

	struct data_packet *packet;
	unsigned char recvmsg[MAXLINE];
	struct sockaddr_un servaddr;
	int len, i, n;
	uint32_t *src_ip_addr;
	char str[INET_ADDRSTRLEN];
	fd_set setallfd;
	struct timeval tv;

	tv.tv_sec = 20;
	tv.tv_usec = 0;
	FD_ZERO(&setallfd);
	FD_SET(sockfd, &setallfd);

//	signal(SIGALRM, sig_alrm);
	packet = (struct data_packet *) malloc(sizeof(struct data_packet));
	memset(recvmsg, 0, MAXLINE);
	len = sizeof(servaddr);
//	alarm(20);

	n = select(sockfd + 1, &setallfd, NULL, NULL, &tv);
	if (n < 0) {
		printf("Error with select listening\n");
		return -1; 
	}

	if (FD_ISSET(sockfd, &setallfd)) {
		if(recvfrom(sockfd, recvmsg, MAXLINE, 0, (struct sockaddr *) &servaddr, &len) < 0 ){
			printf("\nError while sending data");
		} 
		printf("\npacket recieved");
	}
	else{
		return -99;
	}
	packet = (struct data_packet *)recvmsg;
	strcpy(msg, packet->tpkt.payload);

	src_ip_addr = (uint32_t *)packet->src_ip_addr;
	inet_ntop(AF_INET, src_ip_addr, str, INET_ADDRSTRLEN);
//	printf("Source ip address is %s\n", str);	
	strcpy(srcipaddress, str);

	
	*srcport = packet->tpkt.src_port;
return 0;
}
