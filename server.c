#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <signal.h>
#include <sys/time.h>
#include <time.h>
#include "headers.h"

int main()
{

	int sockfd, srcport, filedes, fastflag = 0, n = 0,i;
	struct sockaddr_un servaddr;
	unsigned char nameBuff[MAXLINE], sendmsg[MAXLINE], recvmsg[MAXLINE], ipaddress[20];
	char myName[MAXLINE],srcipaddress[MAXLINE];
	char *ch;
	time_t t;
	struct hostent *hptr;
	unsigned int ip;

/* Create a temp file */
	memset(myName, 0, MAXLINE);
	memset(nameBuff, 0, MAXLINE);
	memset(sendmsg, 0, MAXLINE);
	memset(recvmsg, 0, MAXLINE);
	memset(ipaddress, 0, 20);
	memset(srcipaddress, 0, 20);

	unlink(SERVER_PATH);
/* Binding server with temperory file */
	sockfd = socket(AF_LOCAL, SOCK_DGRAM, 0);
	bzero(&servaddr,sizeof(servaddr));
	servaddr.sun_family = AF_LOCAL;
	strcpy(servaddr.sun_path, SERVER_PATH);

	if ( bind( sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr) ) < 0){
		perror("\nUnable to bind socket");
		unlink(SERVER_PATH);
		exit(0);
	}
	
	if( (i=gethostname(myName,sizeof(myName))) ==-1 )
	{
		printf("Error in getting hostName");
		return -1;
	}
	

	while(1)
	{

	printf("\n Server waiting at msg_recv");
timedout:
	n = msg_recv(sockfd, recvmsg, srcipaddress, &srcport);
	
	if ( n == -99 )
		goto timedout;

	ip = inet_addr(srcipaddress);
	if( (hptr = gethostbyaddr( (char *)&ip,4,AF_INET)) < 0 ){
		printf("\nInvalid address");
		continue;
	}
	printf("\n server at node %s responding to request from %s", myName,hptr->h_name);

	time(&t);
	sprintf(sendmsg ,"%s", ctime(&t));
	if ( msg_send(sockfd, srcipaddress, srcport, sendmsg, 0) < 0 )
		printf("\nError occured in msg_send");
	}
	close(sockfd);
	unlink(servaddr.sun_path);

return 0;
}
