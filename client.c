#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <signal.h>
#include <sys/time.h>
#include "headers.h"

int main()
{

	int sockfd, srcport, filedes, forceflag = 0, n = 0, i, len;
	struct sockaddr_un cliaddr;
	struct hostent *hptr;
	char nameBuff[MAXLINE], ipaddress[20], inputbuff[20],myName[MAXLINE];
	unsigned char  recvmsg[MAXLINE], sendmsg[MAXLINE], srcipaddress[20];
	char *ch, **pptr;
	unsigned char *check="Timestamp";
	char str [INET_ADDRSTRLEN];
/* Create a temp file */
	memset(myName, 0, MAXLINE);
	memset(nameBuff, 0, MAXLINE);
	memset(sendmsg, 0, 20);
	memset(recvmsg, 0, MAXLINE);
	memset(ipaddress, 0, 20);
	memset(srcipaddress, 0, 20);
	strcpy(nameBuff,"101TmpFileXXXXXX");
	strcpy(sendmsg,check);
	filedes = mkstemp(nameBuff);
	if(filedes < 0)
	{
		perror("\nError while creating a file using mkstemp");
		exit(0);
	}
	unlink(nameBuff);

/* Binding client with temperory file */
	sockfd = socket(AF_LOCAL, SOCK_DGRAM, 0);
	bzero(&cliaddr,sizeof(cliaddr));
	cliaddr.sun_family = AF_LOCAL;
	strcpy(cliaddr.sun_path, nameBuff);
	len = sizeof(cliaddr);

	if ( bind( sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr) ) < 0){
		perror("\nUnable to bind socket");
		exit(0);
	}

	while(1){
		forceflag = 0;
		printf("\nSelect your choice\n1. vm1\n2. vm2\n3. vm3\n4. vm4\n5. vm5\n6. vm6\n7. vm7\n8. vm8\n9. vm9\n10.vm10\n11.exit");
		printf("\nEnter the VM number");
		ch = fgets(inputbuff,20,stdin);

		if(!strcmp(inputbuff,"1\n")){
			strcpy(ipaddress,"vm1");
		}
		else if (!strcmp(inputbuff,"2\n")){
			strcpy(ipaddress,"vm2");
		}
		else if (!strcmp(inputbuff,"3\n")){
			strcpy(ipaddress,"vm3");
		}
		else if (!strcmp(inputbuff,"4\n")){
			strcpy(ipaddress,"vm4");
		}
		else if (!strcmp(inputbuff,"5\n")){
			strcpy(ipaddress,"vm5");
		}
		else if (!strcmp(inputbuff,"6\n")){
			strcpy(ipaddress,"vm6");
		}
		else if (!strcmp(inputbuff,"7\n")){
			strcpy(ipaddress,"vm7");
		}
		else if (!strcmp(inputbuff,"8\n")){
			strcpy(ipaddress,"vm8");
		}
		else if (!strcmp(inputbuff,"9\n")){
			strcpy(ipaddress,"vm9");
		}
		else if (!strcmp(inputbuff,"10\n")){
			strcpy(ipaddress,"vm10");
		}
		else if (!strcmp(inputbuff,"11\n"))
			break;
		else{
			printf("wrong choice");
			continue;
		}

/* Get the IP address of the destination VM */
		if((hptr = gethostbyname(ipaddress))){
		switch(hptr->h_addrtype){
			case AF_INET: 
				pptr = hptr->h_addr_list;
				inet_ntop(hptr->h_addrtype, *pptr,str,sizeof(str));
				printf("ip address is %s\n",str);
			break;
			default:
			printf("unknown address");break;
		}
		}
		else{printf("Invalid arguments");
			return -1;
		}

		i = gethostname(myName,sizeof(myName));
		if ( i == -1)
		{
			printf("\nError in hostname");
			return -1;
		}
RouteDiscovery:
		if ( msg_send(sockfd, str, SERVER_PORT, sendmsg, forceflag) < 0 )
			printf("\nError occured in msg_send");
		printf("\nclient at node %s sending request to server at %s",myName,ipaddress);
		n = msg_recv(sockfd, recvmsg, srcipaddress, &srcport);

/* msg_recv returns -99 when timeout occurs. We have to set the forceflag = 1 and send the request again */
		if ( n == -99 )
		{
			printf("\nclient at node  %s : timeout on response from %s", myName,ipaddress);
			if(forceflag){
				printf("\n Tried with forceflag... still timedout");
				continue;
			}
			forceflag = 1;
			goto RouteDiscovery;
//			goto out;
		}

		printf("\nclient at node %s: received from %s: %s",myName,ipaddress,recvmsg);

/*		for (i=0; i<4; i++)
		printf("\nThe ip value is %x",srcipaddress[i]);
		printf("\n port number is %d", srcport);
*/
	}
out:	close(sockfd);
	unlink(cliaddr.sun_path);

return 0;
}
